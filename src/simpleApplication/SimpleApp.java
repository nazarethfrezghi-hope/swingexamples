package simpleApplication;
/*
 * SimpleApp.java
 *
 * Created on September 27, 2002, 12:33 PM
 * Modified 1/16/18. Turned into an application.
 * A simple application to demostrate one way of doing event handling
 * with swing components.
 * It has an input field, an output field, and a button.
 * The user is supposed to enter a number, and the applet puts in the
 * output field the square root of the number, with some text added.
 * The applet also does some exception handling, in case the user
 * inputs something other than a floating point number.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SimpleApp extends JFrame implements ActionListener {
    
    // The various components on the screen
    //
    JTextArea instructions;
    JTextField messages;
    JButton theButton;
    JTextField input; 
    JTextField output;
    
    public static void main(String[] args) {
    	SimpleApp app = new SimpleApp();
    	app.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	app.pack();
    	app.setSize(600,150);
    	app.setVisible(true);
    }
    public SimpleApp() {
        // We need the ContentPane of the applet--this is the thing we place 
        // stuff on.
        Container myContainer=this.getContentPane();

        // Have the container use a BorderLaout to manage the display.
        myContainer.setLayout(new BorderLayout());
    
        // Instantiate the 5 components to be placed on the screen.
        theButton=new JButton("Press me");
        input=new JTextField(10);
        output=new JTextField(30);
        messages=new JTextField();
        instructions=new JTextArea("Enter a number in the box to the left "
                                   +"and press the button");
        
        // Add the components the the container in the appropriate places
        myContainer.add(instructions,BorderLayout.NORTH);
        myContainer.add(input,BorderLayout.WEST);
        myContainer.add(output,BorderLayout.EAST);
        myContainer.add(theButton,BorderLayout.CENTER);
        myContainer.add(messages,BorderLayout.SOUTH);

        // Set this class as the action listener for the button and the input. 
        // Thus, when the button is pressed, or <ENTER> pressed in the text
        // field, this object will be informed of that event.  This is done by
        // a call to the actionPerformed method defined below.
        theButton.addActionListener(this);
        input.addActionListener(this);
    }
    
    //-------------------------------------------------------------------------
    // actionPerformed is from the interface ActionListener.  Since we implement
    // ActionListener, we must define this method.  It is what does the work
    // when an event happens on an object this class is listening to.
    // Since the JButton and the input JTextField asked this to listen for events,
    // whenever they do there thing (button pressed, <ENTER> hit in field), 
    // They will "inform" this object by calling this method.
    //
    public void actionPerformed(ActionEvent e) {

            //---------------------------------------------------------------
            // Since this class is handling multiple events of the same type,
            // we need to know what object sent the event so we can react
            // appropriately.
            //
            if(e.getSource()==theButton) {
                messages.setText("The button was pressed");
            }
            else if(e.getSource()==input) {
                messages.setText("The enter key was pressed in the input field");
            }
            else {
                messages.setText("Something happened, but I don't know what.");
            }

            String theInput=input.getText();
            
            String outputString="";
            float in;

            //---------------------------------------------------------------
            // Here we use a try-catch block in case there is an exception.
            // We only catch one type of exception--a NumberFormatException
            // If another type of exception occurs, the program will crash.
            // However, if a NumberFormatException occurs, instead of crashing,
            // the program will execute what is in the catch block.  The code
            // in a catch block should be written so that the program is able 
            // to "recover" in the event of something unforeseen.
            // In this case, if the text in the input field is not a number,
            // we don't know what to do, since we need a number.  Therefore 
            // we simply ask the user to enter a number.  
            //
            try {
                in=Float.parseFloat(theInput);
                outputString +="The square root of " + in + " is " 
                             + Math.sqrt(in);
                output.setText(outputString);
            }
            catch(NumberFormatException f) {
                messages.setText("You should enter a number, not strings and stuff.");
            }
    }
}
