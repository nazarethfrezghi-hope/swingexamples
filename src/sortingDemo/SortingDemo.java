package sortingDemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

public class SortingDemo extends JFrame {

	int			randList[];
	int			sSortList[];
	int			qSortList[];
	JTextField	randSize	= new JTextField(2);
	JTextArea	randInput	= new JTextArea(1, 60);
	JTextArea	qSortOutput	= new JTextArea(1, 60);
	JTextArea	sSortOutput	= new JTextArea(1, 60);
	
    public static void main(String[] args) {
    	SortingDemo app = new SortingDemo();
    	app.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	app.pack();
    	app.setSize(800,200);
    	app.setVisible(true);
    }
    
	public SortingDemo() {
		// --------------------------------------------------------------------
		// Create input stuff
		JLabel sizeLabel = new JLabel("List size:");
		JButton randButton = new JButton("Random List");
		randInput.setBorder(new CompoundBorder(new LineBorder(Color.GRAY, 4), new LineBorder(Color.BLACK)));
		randInput.setEditable(false);

		Box randBox = Box.createHorizontalBox();
		randBox.add(sizeLabel);
		randBox.add(Box.createHorizontalStrut(10));
		randBox.add(randSize);
		randBox.add(Box.createHorizontalStrut(10));
		randBox.add(randButton);
		randBox.add(Box.createHorizontalStrut(10));
		randBox.add(randInput);

		// --------------------------------------------------------------------
		// Create Sorting buttons and outputs.
		JButton sSortButton = new JButton("Insertion Sort");
		sSortOutput.setEditable(false);
		sSortOutput.setBorder(new CompoundBorder(new LineBorder(Color.ORANGE, 4), new LineBorder(Color.blue, 3)));

		Box sSortBox = Box.createHorizontalBox();
		sSortBox.add(Box.createHorizontalStrut(20));
		sSortBox.add(sSortButton);
		sSortBox.add(Box.createHorizontalStrut(10));
		sSortBox.add(sSortOutput);
		sSortBox.add(Box.createHorizontalStrut(5));

		JButton qSortButton = new JButton("QuickSort");
		qSortOutput.setEditable(false);
		qSortOutput.setBorder(new CompoundBorder(new LineBorder(Color.BLUE, 4), new LineBorder(Color.ORANGE, 3)));

		Box qSortBox = Box.createHorizontalBox();
		qSortBox.add(Box.createHorizontalStrut(20));
		qSortBox.add(qSortButton);
		qSortBox.add(Box.createHorizontalStrut(10));
		qSortBox.add(qSortOutput);
		qSortBox.add(Box.createHorizontalStrut(5));

		// --------------------------------------------------------------------
		// Put the boxes with buttons and lists on another box.
		//
		Box middleBox = Box.createVerticalBox();
		middleBox.add(randBox);
		middleBox.add(sSortBox);
		middleBox.add(qSortBox);


		JButton clearButton = new JButton("Clear All");

		Box clearBox = Box.createHorizontalBox();
		clearBox.add(Box.createHorizontalGlue());
		clearBox.add(clearButton);
		clearBox.add(Box.createHorizontalGlue());

		this.getContentPane().add(middleBox, BorderLayout.CENTER);
		this.getContentPane().add(clearBox, BorderLayout.SOUTH);
		// --------------------------------------------------------------------
		// --------------------------------------------------------------------
		// Now add all of the listeners.
		randButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearStuff();
				int size;
				try {
					size = Integer.parseInt(randSize.getText());
				} catch (NumberFormatException f) {
					size = 1;
				}
				randList(size);
				showList(randList, randInput);
			}
		});
		sSortButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sSortList = sortList(randList);
				showList(sSortList, sSortOutput);
			}
		});
		qSortButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qSortList = qSort(randList);
				showList(qSortList, qSortOutput);
			}
		});
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearStuff();
			}
		});
	}

	void showList(int[] list, JTextArea area) {
		area.setText("");
		if (list != null) {
			for (int i = 0; i < list.length; i++) {
				if (list[i] < 10)
					area.append(" " + list[i] + " ");
				else
					area.append(list[i] + " ");
			}
		}
	}

	void clearStuff() {
		randInput.setText("");
		sSortOutput.setText("");
		qSortOutput.setText("");
		System.out.println("cleared");
	}

	// -------------------------------------------------------------------------
	// The selection sort algorithm
	//
	int[] sortList(int inputArray[]) {
		int[] outputArray = new int[inputArray.length];
		outputArray[0] = inputArray[0];
		for (int i = 1; i < inputArray.length; i++) {
			int j = i - 1;
			while (j >= 0 && outputArray[j] > inputArray[i]) {
				outputArray[j + 1] = outputArray[j];
				j--;
			}
			outputArray[j + 1] = inputArray[i];
		}
		return outputArray;
	}

	// -------------------------------------------------------------------------
	// The quicksort algorithm--first call
	//
	int[] qSort(int inputArray[]) {
		int[] outputArray = new int[inputArray.length];
		for (int i = 0; i < inputArray.length; i++) {
			outputArray[i] = inputArray[i];
		}
		quickSort(outputArray, 0, outputArray.length - 1);
		return outputArray;
	}

	// -------------------------------------------------------------------------
	// The quicksort algorithm--normal algorithm
	//
	void quickSort(int inputArray[], int first, int last) {
		if (first < last) {
			int pivot = partition(inputArray, first, last);
			quickSort(inputArray, first, pivot - 1);
			quickSort(inputArray, pivot + 1, last);
		}
	}

	// -------------------------------------------------------------------------
	// The partition algorithm. Used by Quicksort.
	//
	int partition(int inputArray[], int first, int last) {
		int thePivot = inputArray[first];
		int i = first + 1;
		int j = last;
		int empty = first;
		while (true) {
			while (i <= last && inputArray[i] < thePivot) {
				i++;
			}
			while (j >= first && inputArray[j] > thePivot) {
				j--;
			}
			if (i >= j)
				break;
			inputArray[empty] = inputArray[j];
			inputArray[j] = inputArray[i];
			empty = i;
			i++;
			j--;
		}
		if (j >= empty) {
			inputArray[empty] = inputArray[j];
			inputArray[j] = thePivot;
		} else {
			inputArray[empty] = thePivot;
			j = empty;
		}
		return j;
	}

	// -------------------------------------------------------------------------
	// Create a new rand list of length n.
	//
	void randList(int length) {
		randList = new int[length];
		for (int i = 0; i < length; i++) {
			randList[i] = (int) (Math.random() * 100);
		}
	}
	// -------------------------------------------------------------------------
}
