package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleSwingAppS16_02 {
	JTextField inField;
	private JTextArea outArea;

	public SimpleSwingAppS16_02() {
		JFrame frame = new JFrame("Our simple app");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container cont = frame.getContentPane();
		cont.setLayout(new BorderLayout());

		JButton button = new JButton("Append");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String s = inField.getText();
				outArea.append("\n" + s);
			}
		});

		inField = new JTextField();

		Box inBox = Box.createHorizontalBox();
		inBox.add(button);
		inBox.add(inField);
		cont.add(inBox, BorderLayout.NORTH);

		outArea = new JTextArea();
		outArea.setEditable(false);
		cont.add(outArea, BorderLayout.CENTER);

		frame.pack();
		frame.setSize(400, 300);

		frame.setVisible(true);

	}

	public static void main(String[] args) {
		new SimpleSwingAppS16_02();
	}

}
